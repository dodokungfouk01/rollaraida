﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerFall : MonoBehaviour
{
    private GameObject SavePos;
    public float Delay;
    private float SaveDelay;
    public float RespawnTime;
    private float SaveRespawnTime;
    private bool Check;
    
    void OnCollisionEnter(Collision col)
    {
        if(col.gameObject.CompareTag("Player"))
        {
            
            Check=true;
        }
    }
    void Start()
    {
        SavePos = new GameObject();
        SavePos.transform.position=this.transform.position;
        SavePos.transform.rotation=this.transform.rotation;
        SaveDelay=Delay;
        SaveRespawnTime=RespawnTime;
        
    }
    void Update()
    {
        if(Check) 
        {
            Delay-=Time.deltaTime;
            if(Delay<=0)
            {
                 GetComponent<Rigidbody>().isKinematic=false;
                 RespawnTime-=Time.deltaTime;
                 if(RespawnTime<=0)
                 {
                      GetComponent<Rigidbody>().isKinematic=true;
                      this.transform.position=SavePos.transform.position;
                      this.transform.rotation=SavePos.transform.rotation;
                      Check=false;
                      Delay=SaveDelay;
                      RespawnTime=SaveRespawnTime;
                      
                 }
            }
        }

    }
}
