﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

//Adding this allows us to access members of the UI namespace including Text.
using UnityEngine.UI;

public class CompletePlayerController : MonoBehaviour {

	public float speed;				//Floating point variable to store the player's movement speed.
	//public Text countText;			//Store a reference to the UI Text component which will display the number of pickups collected.
	//public Text winText;			//Store a reference to the UI Text component which will display the 'You win' message.

	private Rigidbody rb;	
		

	
	void Start()
	{
		
		rb = GetComponent<Rigidbody> ();

		
	}

	
	void FixedUpdate()
	{
		
		float moveHorizontal = Input.GetAxis ("Horizontal");

		//Store the current vertical input in the float moveVertical.
		float moveVertical = Input.GetAxis ("Vertical");
		

		//Use the two store floats to create a new Vector2 variable movement.
		Vector3 movement = new Vector3 (moveHorizontal,0, moveVertical);

		//Call the AddForce function of our Rigidbody2D rb2d supplying movement multiplied by speed to move our player.
		rb.AddForce (movement * speed);
		if(Input.GetKey(KeyCode.R))
		{
			
			SceneManager.LoadScene("Main");
		}
		
		
	}
	public void FixSpeedDown()
	{
		speed = 10;
	}
	public void FixSuperSpeedDown()
	{
		speed = -5f;
	}
	public void FixSpeedUp()
	{
		speed = 40;
	}
	public void Flying()
	{
		rb.AddForce (0,55,0);
	}
	public void Flying1()
	{
		rb.AddForce (0,2000,0);
	}
	public void ForceRight()
	{
		rb.AddForce (250,0,0);
	}
	public void ForceLeft()
	{
		rb.AddForce (-250,0,0);
	}
	public void KnockBack()
	{
		rb.AddForce (0,0,-1000);
	}
	

	

	

	//This function updates the text displaying the number of objects we've collected and displays our victory message if we've collected all of them.
	
}
